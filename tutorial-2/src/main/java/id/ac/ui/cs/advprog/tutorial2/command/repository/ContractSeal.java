package id.ac.ui.cs.advprog.tutorial2.command.repository;

import id.ac.ui.cs.advprog.tutorial2.command.core.spell.Spell;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class ContractSeal {

    private Map<String, Spell> spells;
    private Spell latestSpell;
    private boolean alreadyUndo = false;

    public ContractSeal() {
        spells = new HashMap<>();
    }

    public void registerSpell(Spell spell) {
        spells.put(spell.spellName(), spell);
    }

    public void castSpell(String spellName) {
        // TODO: Complete Me
        latestSpell = spells.get(spellName);
        latestSpell.cast();
        alreadyUndo = false;
    }

    public void undoSpell() {
        // TODO: Complete Me
        if(alreadyUndo == false){
            latestSpell.undo();
            alreadyUndo = true;
        }
    }

    public Collection<Spell> getSpells() { return spells.values(); }
}
