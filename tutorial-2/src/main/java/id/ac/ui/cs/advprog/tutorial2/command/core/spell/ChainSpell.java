package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.ArrayList;

public class ChainSpell implements Spell {
    // TODO: Complete Me
    protected ArrayList<Spell> spells;
    private boolean alreadyUndo = false;
    public ChainSpell(ArrayList<Spell> spells){
        this.spells = spells;
    }

    @Override
    public void cast() {
        for(Spell spell : spells){
            spell.cast();
        }
        alreadyUndo = false;
    }

    @Override
    public void undo() {
        if(alreadyUndo == false){
            for(int i = spells.size() - 1; i >= 0; i--){
                spells.get(i).undo();
            }
            alreadyUndo = true;
        }
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }
}
