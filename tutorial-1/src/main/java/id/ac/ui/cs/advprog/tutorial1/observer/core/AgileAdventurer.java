package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class AgileAdventurer extends Adventurer {


    public AgileAdventurer(Guild guild) {
        this.name = "Agile";
        //ToDo: Complete Me
        this.guild = guild;
    }

    //ToDo: Complete Me
    @Override
    public void update() {
        if (guild.getQuestType().equalsIgnoreCase("d")) {
            getQuests().add(guild.getQuest());
        } else if (guild.getQuestType().equalsIgnoreCase("r")) {
            getQuests().add(guild.getQuest());
        } else {

        }
    }
}
