package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.RunicCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.CodexTranslator;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;

public class Transformation {

    private AbyssalTransformation abyss;
    private CelestialTransformation celestial;
    private GaiusTransformation gaius;
    private AlphaCodex alphaCodex = AlphaCodex.getInstance();
    private RunicCodex runicCodex = RunicCodex.getInstance();


    public Transformation(){
        this.abyss = new AbyssalTransformation();
        this.celestial = new CelestialTransformation();
        this.gaius = new GaiusTransformation();
    }
    
    public String encode(String text){
        Spell spell = new Spell(text, alphaCodex);
        spell = gaius.encode(spell);
        spell = abyss.encode(spell);
        spell = celestial.encode(spell);

        return CodexTranslator.translate(spell, runicCodex).getText();
    }

    public String decode(String code){
        Spell spell = new Spell(code, runicCodex);
        spell = celestial.decode(spell);
        spell = abyss.decode(spell);
        spell = gaius.decode(spell);

        return CodexTranslator.translate(spell, alphaCodex).getText();
    }
}
