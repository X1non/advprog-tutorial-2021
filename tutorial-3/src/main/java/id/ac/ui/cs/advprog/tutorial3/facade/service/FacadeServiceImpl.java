package id.ac.ui.cs.advprog.tutorial3.facade.service;

import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.Transformation;
import org.springframework.stereotype.Service;

/*
 * Asumsikan kelas ini sebagai kelas Client
*/
@Service
public class FacadeServiceImpl implements FacadeService {

    private boolean recentRequestType;
    private String recentRequestValue;
    private Transformation transformation = new Transformation();

    @Override
    public String encode(String text){
        return transformation.encode(text);
    }

    @Override
    public String decode(String code){
        return transformation.decode(code);
    }

    @Override
    public void setRequestType(String requestType){
        recentRequestType = requestType.equals("encode");
    }

    @Override
    public void setRequestValue(String requestValue){
        recentRequestValue = requestValue;
    }

    @Override
    public boolean isRequestEncode(){
        return recentRequestType;
    }

    @Override
    public String getRequestValue(){
        return recentRequestValue;
    }
}
