package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;

/**
 * Caesar chiper
 */
public class GaiusTransformation {
    private int key;

    public GaiusTransformation(){
        this.key = 3;
    }

    public Spell encode(Spell spell){
        return process(spell, true);
    }

    public Spell decode(Spell spell){
        return process(spell, false);
    }

    private Spell process(Spell spell, boolean encode){
        int selector = encode ? 1 : -1;
        String text = spell.getText();
        Codex codex = spell.getCodex();
        int n = text.length();
        char[] res = new char[n];
        for(int i = 0; i < n; i++){
            int newIdx = codex.getIndex(text.charAt(i)) + key * selector;
            if(newIdx < 0){
                newIdx += codex.getCharSize();
            } else if (newIdx >= codex.getCharSize()) {
                newIdx -= codex.getCharSize();
            }
            res[i] = codex.getChar(newIdx);
        }
        return new Spell(new String(res), codex);
    }
}
