package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

// TODO: complete me :)
public class SpellbookAdapter implements Weapon {

    private Spellbook spellbook;
    private boolean alreadyCharge;

    public SpellbookAdapter(Spellbook spellbook) {
        this.spellbook = spellbook;
        this.alreadyCharge = false;
    }

    @Override
    public String normalAttack() {
        alreadyCharge = false;
        return spellbook.smallSpell();
    }

    @Override
    public String chargedAttack() {
        if(!alreadyCharge){
            this.alreadyCharge = true;
            return spellbook.largeSpell();
        } else {
            return "Not enough energy...";
        }
    }

    @Override
    public String getName() {
        return spellbook.getName();
    }

    @Override
    public String getHolderName() {
        return spellbook.getHolderName();
    }

}
