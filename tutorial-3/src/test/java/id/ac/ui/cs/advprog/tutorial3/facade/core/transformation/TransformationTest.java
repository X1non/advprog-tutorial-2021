package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;

public class TransformationTest {
    private Class<?> transformationClass;

    @BeforeEach
    public void setup() throws Exception {
        transformationClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.Transformation");
    }

    @Test
    public void testTransformationIsAPublicClass() {
        int classModifiers = transformationClass.getModifiers();
        assertTrue(Modifier.isPublic(classModifiers));
    }

    @Test
    public void testTransformationInitHasRequiredAttributes(){
        Transformation transformation = new Transformation();
        assertNotNull(transformation);
    }

    @Test
    public void testTransformationHasEncodeMethod() throws Exception {
        Method translate = transformationClass.getDeclaredMethod("encode", String.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testTransformationEncodesProperly() throws Exception {
        String text = "Safira and I went to a blacksmith to forge our sword";
        String expected = "#H#[ZpxDGGB,C]Gopr@CV*y#S]B+n@nB{[{H[Z>D#yCSSFA;%@^J";

        String result = new Transformation().encode(text);
        assertEquals(expected, result);
    }

    @Test
    public void testTransformationHasDecodeMethod() throws Exception {
        Method translate = transformationClass.getDeclaredMethod("decode", String.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testTransformationDecodesProperly() throws Exception {
        String code = "#H#[ZpxDGGB,C]Gopr@CV*y#S]B+n@nB{[{H[Z>D#yCSSFA;%@^J";
        String expected = "Safira and I went to a blacksmith to forge our sword";

        String result = new Transformation().decode(code);
        assertEquals(expected, result);
    }
}
