package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class GaiusTransformationTest {
    private Class<?> gaiusClass;

    @BeforeEach
    public void setup() throws Exception {
        gaiusClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.GaiusTransformation");
    }

    @Test
    public void testGaiusHasEncodeMethod() throws Exception {
        Method translate = gaiusClass.getDeclaredMethod("encode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testGaiusEncodesCorrectly() throws Exception {
        String text = "Safira and I went to a blacksmith to forge our sword";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "VdiludCdqgCLCzhqwCwrCdCeodfnvplwkCwrCirujhCrxuCvzrug";

        Spell result = new GaiusTransformation().encode(spell);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testGaiusHasDecodeMethod() throws Exception {
        Method translate = gaiusClass.getDeclaredMethod("decode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testGaiusDecodesCorrectly() throws Exception {
        String text = "VdiludCdqgCLCzhqwCwrCdCeodfnvplwkCwrCirujhCrxuCvzrug";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "Safira and I went to a blacksmith to forge our sword";

        Spell result = new GaiusTransformation().decode(spell);
        assertEquals(expected, result.getText());
    }
}
