package id.ac.ui.cs.advprog.tutorial4.factory.service;

import id.ac.ui.cs.advprog.tutorial4.factory.core.factory.*;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.InuzumaRamen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.LiyuanSoba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.MondoUdon;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.SnevnezhaShirataki;
import id.ac.ui.cs.advprog.tutorial4.factory.repository.MenuRepository;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class MenuServiceImpl implements MenuService {
    private MenuRepository repo;

    public MenuServiceImpl(MenuRepository repo){
        this.repo = repo;
        initRepo();
    }

    public MenuServiceImpl(){
        this(new MenuRepository());
    }

    public Menu createMenu(String name, String type){
        MenuFactory liyuanSobaFactory = new LiyuanSobaFactory();
        MenuFactory inuzumaRamenFactory = new InuzumaRamenFactory();
        MenuFactory mondoUdonFactory = new MondoUdonFactory();
        MenuFactory snevnezhaShiratakiFactory = new SnevnezhaShiratakiFactory();
        Menu menu = null;

        if(type.equals("LiyuanSoba")){
            menu = new Menu(name, liyuanSobaFactory);
        } else if(type.equals("InuzumaRamen")){
            menu = new Menu(name, inuzumaRamenFactory);
        } else if(type.equals("MondoUdon")) {
            menu = new Menu(name, mondoUdonFactory);
        } else {
            menu = new Menu(name, snevnezhaShiratakiFactory);
        }
        return repo.add(menu);
    }

    public List<Menu> getMenus(){
        return repo.getMenus();
    }

    private void initRepo(){
        createMenu("WanPlus Beef Mushroom Soba", "LiyuanSoba");
        createMenu("Bakufu Spicy Pork Ramen", "InuzumaRamen");
        createMenu("Good Hunter Cheese Chicken Udon", "MondoUdon");
        createMenu("Morepeko Flower Fish Shirataki", "SnevnezhaShirataki");
    }
}