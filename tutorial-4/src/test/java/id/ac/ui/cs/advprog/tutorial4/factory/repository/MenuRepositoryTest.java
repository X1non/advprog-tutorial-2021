package id.ac.ui.cs.advprog.tutorial4.factory.repository;

import id.ac.ui.cs.advprog.tutorial4.factory.core.factory.MondoUdonFactory;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.test.util.ReflectionTestUtils;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;


import static org.assertj.core.api.Assertions.assertThat;


public class MenuRepositoryTest {

    private MenuRepository menuRepository;
    private Menu sampleMenu;

    @Mock
    private List<Menu> menus;

    @Mock
    private MondoUdonFactory mondoUdonFactory = new MondoUdonFactory();

    @BeforeEach
    public void setUp() {
        menuRepository = new MenuRepository();
        menus = new ArrayList<>();
        sampleMenu = new Menu("Ramenano", mondoUdonFactory);
        menus.add(sampleMenu);
    }

    @Test
    public void whenMenuRepoGetMenuItShouldReturnMenuList() {
        ReflectionTestUtils.setField(menuRepository, "list", menus);
        List<Menu> acquiredMenus = menuRepository.getMenus();
        assertThat(acquiredMenus).isEqualTo(new ArrayList<>(menus));
    }

    @Test
    public void whenMenuRepoAddItShouldSaveMenu() {
        menuRepository.add(sampleMenu);
        for(Menu menu : menuRepository.getMenus()){
            if(menu.equals(sampleMenu)){
                assertThat(menu).isEqualTo(sampleMenu);
            }
        }
    }
}
