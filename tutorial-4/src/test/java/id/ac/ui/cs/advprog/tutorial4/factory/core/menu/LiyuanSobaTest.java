package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Sweet;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Beef;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Soba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Mushroom;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.LiyuanSoba;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class LiyuanSobaTest {
    private Class<?> liyuanSobaClass;
    private LiyuanSoba liyuanSoba;
    private Menu menu;

    @BeforeEach
    public void setUp() throws Exception {
        liyuanSobaClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.LiyuanSoba");
        liyuanSoba = new LiyuanSoba("Sobanano");
    }

    @Test
    public void testLiyuanSobaIsConcreteClass() {
        assertFalse(Modifier.isAbstract(liyuanSobaClass.getModifiers()));
    }

    @Test
    public void testLiyuanSobaIsAMenu() {
        Collection<Type> superClass = Arrays.asList(liyuanSobaClass.getSuperclass());

        assertTrue(superClass.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu")));
    }

    @Test
    public void whenInitItShouldReturnCorrectString() {
        assertEquals("Sobanano", liyuanSoba.getName());
        assertEquals("Adding Liyuan Soba Noodles...", liyuanSoba.getNoodle().getDescription());
        assertEquals("Adding Maro Beef Meat...", liyuanSoba.getMeat().getDescription());
        assertEquals("Adding a dash of Sweet Soy Sauce...", liyuanSoba.getFlavor().getDescription());
        assertEquals("Adding Shiitake Mushroom Topping...", liyuanSoba.getTopping().getDescription());
    }
}
