package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class MondoUdonTest {
    private Class<?> mondoUdonClass;
    private MondoUdon mondoUdon;
    private Menu menu;

    @BeforeEach
    public void setUp() throws Exception {
        mondoUdonClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.MondoUdon");
        mondoUdon = new MondoUdon("Udonano");
    }

    @Test
    public void testMondoUdonIsConcreteClass() {
        assertFalse(Modifier.isAbstract(mondoUdonClass.getModifiers()));
    }

    @Test
    public void testMondoUdonIsAMenu() {
        Collection<Type> superClass = Arrays.asList(mondoUdonClass.getSuperclass());

        assertTrue(superClass.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu")));
    }

    @Test
    public void whenInitItShouldReturnCorrectString() {
        assertEquals("Udonano", mondoUdon.getName());
        assertEquals("Adding Mondo Udon Noodles...", mondoUdon.getNoodle().getDescription());
        assertEquals("Adding Wintervale Chicken Meat...", mondoUdon.getMeat().getDescription());
        assertEquals("Adding a pinch of salt...", mondoUdon.getFlavor().getDescription());
        assertEquals("Adding Shredded Cheese Topping...", mondoUdon.getTopping().getDescription());
    }

}
