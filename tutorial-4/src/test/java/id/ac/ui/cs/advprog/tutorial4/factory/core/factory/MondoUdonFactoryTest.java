package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

public class MondoUdonFactoryTest {
    private Class<?> mondoUdonFactoryClass;
    private MondoUdonFactory mondoUdonFactory;

    @BeforeEach
    public void setUp() throws Exception {
        mondoUdonFactoryClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.MondoUdonFactory");
        mondoUdonFactory = new MondoUdonFactory();
    }

    @Test
    public void testMondoUdonFactoryIsConcreteClass() {
        assertFalse(Modifier.isAbstract(mondoUdonFactoryClass.getModifiers()));
    }

    @Test
    public void testMondoUdonFactoryIsAMenuFactory() {
        Collection<Type> interfaces = Arrays.asList(mondoUdonFactoryClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.MenuFactory")));
    }

    @Test
    public void testMondoUdonFactoryOverrideCreateNoodleMethod() throws Exception {
        Method createNoodle = mondoUdonFactoryClass.getDeclaredMethod("createNoodle");
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle", createNoodle.getGenericReturnType().getTypeName());
        assertEquals(0, createNoodle.getParameterCount());
        assertTrue(Modifier.isPublic(createNoodle.getModifiers()));
    }

    @Test
    public void testMondoUdonFactoryOverrideCreateFlavorMethod() throws Exception {
        Method createFlavor = mondoUdonFactoryClass.getDeclaredMethod("createFlavor");
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor", createFlavor.getGenericReturnType().getTypeName());
        assertEquals(0, createFlavor.getParameterCount());
        assertTrue(Modifier.isPublic(createFlavor.getModifiers()));
    }

    @Test
    public void testMondoUdonFactoryOverrideGetMeatMethod() throws Exception {
        Method getMeat = mondoUdonFactoryClass.getDeclaredMethod("createMeat");
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat", getMeat.getGenericReturnType().getTypeName());
        assertEquals(0, getMeat.getParameterCount());
        assertTrue(Modifier.isPublic(getMeat.getModifiers()));
    }

    @Test
    public void testMondoUdonFactoryOverrideGetToppingMethod() throws Exception {
        Method getTopping = mondoUdonFactoryClass.getDeclaredMethod("createTopping");
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping", getTopping.getGenericReturnType().getTypeName());
        assertEquals(0, getTopping.getParameterCount());
        assertTrue(Modifier.isPublic(getTopping.getModifiers()));
    }
}
