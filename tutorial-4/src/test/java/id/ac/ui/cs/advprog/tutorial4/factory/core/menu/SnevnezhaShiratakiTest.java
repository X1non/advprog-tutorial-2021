package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class SnevnezhaShiratakiTest {
    private Class<?> snevnezhaShiratakiClass;
    private SnevnezhaShirataki snevnezhaShirataki;
    private Menu menu;

    @BeforeEach
    public void setUp() throws Exception {
        snevnezhaShiratakiClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.SnevnezhaShirataki");
        snevnezhaShirataki = new SnevnezhaShirataki("Snevnano");
    }

    @Test
    public void testSnevnezhaShiratakiIsConcreteClass() {
        assertFalse(Modifier.isAbstract(snevnezhaShiratakiClass.getModifiers()));
    }

    @Test
    public void testSnevnezhaShiratakiIsAMenu() {
        Collection<Type> superClass = Arrays.asList(snevnezhaShiratakiClass.getSuperclass());

        assertTrue(superClass.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu")));
    }

    @Test
    public void whenInitItShouldReturnCorrectString() {
        assertEquals("Snevnano", snevnezhaShirataki.getName());
        assertEquals("Adding Snevnezha Shirataki Noodles...", snevnezhaShirataki.getNoodle().getDescription());
        assertEquals("Adding Zhangyun Salmon Fish Meat...", snevnezhaShirataki.getMeat().getDescription());
        assertEquals("Adding WanPlus Specialty MSG flavoring...", snevnezhaShirataki.getFlavor().getDescription());
        assertEquals("Adding Xinqin Flower Topping...", snevnezhaShirataki.getTopping().getDescription());
    }
}
