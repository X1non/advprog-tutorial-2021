package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

public class InuzumaRamenFactoryTest {
    private Class<?> inuzumaRamenFactoryClass;
    private InuzumaRamenFactory inuzumaRamenFactory;

    @BeforeEach
    public void setUp() throws Exception {
        inuzumaRamenFactoryClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.InuzumaRamenFactory");
        inuzumaRamenFactory = new InuzumaRamenFactory();
    }

    @Test
    public void testInuzumaRamenFactoryIsConcreteClass() {
        assertFalse(Modifier.isAbstract(inuzumaRamenFactoryClass.getModifiers()));
    }

    @Test
    public void testInuzumaRamenFactoryIsAMenuFactory() {
        Collection<Type> interfaces = Arrays.asList(inuzumaRamenFactoryClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.MenuFactory")));
    }

    @Test
    public void testInuzumaRamenFactoryOverrideCreateNoodleMethod() throws Exception {
        Method createNoodle = inuzumaRamenFactoryClass.getDeclaredMethod("createNoodle");
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle", createNoodle.getGenericReturnType().getTypeName());
        assertEquals(0, createNoodle.getParameterCount());
        assertTrue(Modifier.isPublic(createNoodle.getModifiers()));
    }

    @Test
    public void testInuzumaRamenFactoryOverrideCreateFlavorMethod() throws Exception {
        Method createFlavor = inuzumaRamenFactoryClass.getDeclaredMethod("createFlavor");
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor", createFlavor.getGenericReturnType().getTypeName());
        assertEquals(0, createFlavor.getParameterCount());
        assertTrue(Modifier.isPublic(createFlavor.getModifiers()));
    }

    @Test
    public void testInuzumaRamenFactoryOverrideGetMeatMethod() throws Exception {
        Method getMeat = inuzumaRamenFactoryClass.getDeclaredMethod("createMeat");
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat", getMeat.getGenericReturnType().getTypeName());
        assertEquals(0, getMeat.getParameterCount());
        assertTrue(Modifier.isPublic(getMeat.getModifiers()));
    }

    @Test
    public void testInuzumaRamenFactoryOverrideGetToppingMethod() throws Exception {
        Method getTopping = inuzumaRamenFactoryClass.getDeclaredMethod("createTopping");
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping", getTopping.getGenericReturnType().getTypeName());
        assertEquals(0, getTopping.getParameterCount());
        assertTrue(Modifier.isPublic(getTopping.getModifiers()));
    }
}
