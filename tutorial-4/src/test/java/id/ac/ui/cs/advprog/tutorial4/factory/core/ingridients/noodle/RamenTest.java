package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class RamenTest {
    private Class<?> ramenClass;
    private Ramen ramen;

    @BeforeEach
    public void setUp() throws Exception {
        ramenClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen");
        ramen = new Ramen();
    }

    @Test
    public void testRamenIsConcreteClass() {
        assertFalse(Modifier.isAbstract(ramenClass.getModifiers()));
    }

    @Test
    public void testRamenIsANoodle() {
        Collection<Type> interfaces = Arrays.asList(ramenClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle")));
    }

    @Test
    public void testRamenOverrideGetDescriptionMethod() throws Exception {
        Method getDescription = ramenClass.getDeclaredMethod("getDescription");
        assertEquals("java.lang.String",getDescription.getGenericReturnType().getTypeName());
        assertEquals(0, getDescription.getParameterCount());
        assertTrue(Modifier.isPublic(getDescription.getModifiers()));
    }

    @Test
    public void whenRamenGetDescriptionItShouldReturnCorrectString() throws Exception {
        assertEquals("Adding Inuzuma Ramen Noodles...", ramen.getDescription());
    }
}
