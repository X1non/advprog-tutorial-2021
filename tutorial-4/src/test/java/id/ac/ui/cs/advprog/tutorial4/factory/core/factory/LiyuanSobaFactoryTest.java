package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

public class LiyuanSobaFactoryTest {
    private Class<?> liyuanSobaFactoryClass;
    private LiyuanSobaFactory liyuanSobaFactory;

    @BeforeEach
    public void setUp() throws Exception {
        liyuanSobaFactoryClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.LiyuanSobaFactory");
        liyuanSobaFactory = new LiyuanSobaFactory();
    }

    @Test
    public void testLiyuanSobaFactoryIsConcreteClass() {
        assertFalse(Modifier.isAbstract(liyuanSobaFactoryClass.getModifiers()));
    }

    @Test
    public void testLiyuanSobaFactoryIsAMenuFactory() {
        Collection<Type> interfaces = Arrays.asList(liyuanSobaFactoryClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.MenuFactory")));
    }

    @Test
    public void testLiyuanSobaFactoryOverrideCreateNoodleMethod() throws Exception {
        Method createNoodle = liyuanSobaFactoryClass.getDeclaredMethod("createNoodle");
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle", createNoodle.getGenericReturnType().getTypeName());
        assertEquals(0, createNoodle.getParameterCount());
        assertTrue(Modifier.isPublic(createNoodle.getModifiers()));
    }

    @Test
    public void testLiyuanSobaFactoryOverrideCreateFlavorMethod() throws Exception {
        Method createFlavor = liyuanSobaFactoryClass.getDeclaredMethod("createFlavor");
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor", createFlavor.getGenericReturnType().getTypeName());
        assertEquals(0, createFlavor.getParameterCount());
        assertTrue(Modifier.isPublic(createFlavor.getModifiers()));
    }

    @Test
    public void testLiyuanSobaFactoryOverrideGetMeatMethod() throws Exception {
        Method getMeat = liyuanSobaFactoryClass.getDeclaredMethod("createMeat");
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat", getMeat.getGenericReturnType().getTypeName());
        assertEquals(0, getMeat.getParameterCount());
        assertTrue(Modifier.isPublic(getMeat.getModifiers()));
    }

    @Test
    public void testLiyuanSobaFactoryOverrideGetToppingMethod() throws Exception {
        Method getTopping = liyuanSobaFactoryClass.getDeclaredMethod("createTopping");
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping", getTopping.getGenericReturnType().getTypeName());
        assertEquals(0, getTopping.getParameterCount());
        assertTrue(Modifier.isPublic(getTopping.getModifiers()));
    }
}
