package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.factory.InuzumaRamenFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

public class InuzumaRamenTest {
    private Class<?> inuzumaRamenClass;
    private InuzumaRamen inuzumaRamen;
    private Menu menu;

    @BeforeEach
    public void setUp() throws Exception {
        inuzumaRamenClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.InuzumaRamen");
        inuzumaRamen = new InuzumaRamen("Ramenano");
    }

    @Test
    public void testInuzumaRamenIsConcreteClass() {
        assertFalse(Modifier.isAbstract(inuzumaRamenClass.getModifiers()));
    }

    @Test
    public void testInuzumaRamenIsAMenu() {
        Collection<Type> superClass = Arrays.asList(inuzumaRamenClass.getSuperclass());

        assertTrue(superClass.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu")));
    }

    @Test
    public void whenInitItShouldReturnCorrectString() {
        assertEquals("Ramenano", inuzumaRamen.getName());
        assertEquals("Adding Inuzuma Ramen Noodles...", inuzumaRamen.getNoodle().getDescription());
        assertEquals("Adding Tian Xu Pork Meat...", inuzumaRamen.getMeat().getDescription());
        assertEquals("Adding Liyuan Chili Powder...", inuzumaRamen.getFlavor().getDescription());
        assertEquals("Adding Guahuan Boiled Egg Topping", inuzumaRamen.getTopping().getDescription());
    }
}
