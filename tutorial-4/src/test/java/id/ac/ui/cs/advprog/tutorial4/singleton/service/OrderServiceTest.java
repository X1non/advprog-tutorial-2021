package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderDrink;
import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderFood;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

public class OrderServiceTest {
    private Class<?> orderServiceClass;
    private OrderServiceImpl orderService;
    private OrderDrink orderDrink;
    private OrderFood orderFood;

    @BeforeEach
    public void setup() throws Exception {
        orderServiceClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial4.singleton.service.OrderServiceImpl");
        orderService = new OrderServiceImpl();
        orderDrink = OrderDrink.getInstance();
        orderFood = OrderFood.getInstance();
    }

    @Test
    public void testOrderServiceImplIsAConcreteClass() {
        assertFalse(Modifier.isAbstract(orderServiceClass.getModifiers()));
    }

    @Test
    public void testOrderServiceImplInheritsFromOrderService() {
        Collection<Type> interfaces = Arrays.asList(orderServiceClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.singleton.service.OrderService")));
    }

    @Test
    public void testOrderServiceHasOrderDrinkMethod() throws Exception {
        Class<?>[] orderDrinkArgs = new Class[1];
        orderDrinkArgs[0] = String.class;

        Method orderDrink = orderServiceClass.getDeclaredMethod("orderADrink", orderDrinkArgs);
        int methodModifiers = orderDrink.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));

    }

    @Test
    public void testOrderServiceHasOrderFoodMethod() throws Exception {
        Class<?>[] orderFoodArgs = new Class[1];
        orderFoodArgs[0] = String.class;

        Method orderFood = orderServiceClass.getDeclaredMethod("orderAFood", orderFoodArgs);
        int methodModifiers = orderFood.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));

    }

    @Test
    public void testOrderServiceHasGetDrinkMethod() throws Exception {
        Method getDrink = orderServiceClass.getDeclaredMethod("getDrink");
        int methodModifiers = getDrink.getModifiers();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderDrink", getDrink.getGenericReturnType().getTypeName());
        assertEquals(0, getDrink.getParameterCount());
        assertTrue(Modifier.isPublic(methodModifiers));

    }

    @Test
    public void testOrderServiceHasGetFoodMethod() throws Exception {
        Method getFood = orderServiceClass.getDeclaredMethod("getFood");
        int methodModifiers = getFood.getModifiers();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderFood", getFood.getGenericReturnType().getTypeName());
        assertEquals(0, getFood.getParameterCount());
        assertTrue(Modifier.isPublic(methodModifiers));

    }

    @Test
    public void testOrderingDrinkSchema(){
        orderDrink.setDrink("Apel Cider");
        assertEquals("Apel Cider", orderService.getDrink().getDrink());
        orderService.orderADrink("RedBull");
        assertEquals("RedBull", orderService.getDrink().getDrink());
    }

    @Test
    public void testOrderingFoodSchema(){
        orderFood.setFood("Hamborger");
        assertEquals("Hamborger", orderService.getFood().getFood());
        orderService.orderAFood("Chimken Nuggets");
        assertEquals("Chimken Nuggets", orderService.getFood().getFood());
    }

}
