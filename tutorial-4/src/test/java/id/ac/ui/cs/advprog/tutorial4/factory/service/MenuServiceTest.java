package id.ac.ui.cs.advprog.tutorial4.factory.service;

import id.ac.ui.cs.advprog.tutorial4.factory.core.factory.MenuFactory;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu;
import id.ac.ui.cs.advprog.tutorial4.factory.repository.MenuRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;

public class MenuServiceTest {
    private Class<?> menuServiceClass;

    @Mock
    Menu menu;

    @Mock
    MenuRepository menuRepository = new MenuRepository();

    @InjectMocks
    MenuService menuService = new MenuServiceImpl();

    @BeforeEach
    public void setup() throws Exception {
        menuServiceClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial4.factory.service.MenuServiceImpl");
    }

    @Test
    public void testMenuServiceHasGetMenusMethod() throws Exception {
        Method getMenus = menuServiceClass.getDeclaredMethod("getMenus");
        int methodModifiers = getMenus.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testMenuServiceHasCreateMenuMethod() throws Exception {
        Class<?>[] createMenuArgs = new Class[2];
        createMenuArgs[0] = String.class;
        createMenuArgs[1] = String.class;

        Method createMenu = menuServiceClass.getDeclaredMethod("createMenu", createMenuArgs);
        int methodModifiers = createMenu.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));

    }

    @Test
    public void testMenuServiceCreateMenuSavedToRepo() {
        assertEquals(4, menuService.getMenus().size());
        menuService.createMenu("Ramenano", "LiyuanSoba");
        menuService.createMenu("Mondono", "MondoUdon");
        assertEquals(6, menuService.getMenus().size());
    }

    @Test
    public void testMenuServiceHasInitRepoMethod() throws Exception {
        Method initRepo = menuServiceClass.getDeclaredMethod("initRepo");
        int methodModifiers = initRepo.getModifiers();
        assertTrue(Modifier.isPrivate(methodModifiers));

    }

}
