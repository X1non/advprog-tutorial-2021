Lazy Instatntiation Approach
-
Pendekatan inisialisasi objek yang dilakukan ketika objek diminta/dipanggil oleh user.

####Keuntungan 
+ Meningkatkan performance *startup* program akibat pengurangan proses yang tidak dibutuhkan/digunakan.

####Kerugian
+ Membutukan kode yang lebih panjang, membuat program terlihat lebih kompleks.
+ *Execution time* yang lebih *consuming* ketika pada akhirnya objek pertama kali diakses.


Eager Instantiation Approach
-
Pendekatan inisialisasi objek yang dilakukan bersamaan ketika program pertama kali dijalankan.

####Keuntungan
+ *Thread safe*. Pemanggilan objek secara bersamaan pasti akan selalu mengembalikan objek yang sudah ter-inisialisasi saat permulaan program dijalankan,
memastikan program yang melibatkan objek tersebut dapat berjalan dengan baik.
+ Respon program terhadap pengeksekusian objek lebih cepat dibandingkan pendekatan *lazy*.

####Kerugian
+ Memakan resource yang membuat *start up* program cenderung lebih lama akibat proses insialisasi objek tersebut. 